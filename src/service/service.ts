import axios from "axios";

export interface EcoCardBase {
  id: number;
  name: string;
  groups?: number;
  shops?: number;
  accounts?: number;
  base: any;
  account: any;
};

export interface BaseIds {
  idGroup: string,
  idCompany: number,
  idShop: string,
}

type Records = BaseIds & EcoCardBase;

export type Accounts = Omit<Records, 'groups' | 'shops' | 'accounts'>

export type Groups = Omit<Records, 'groups' | 'idGroup' | 'idShop'>

export type Shops = Omit<Records, 'idShop' | 'groups' | 'shops'>

const getFileUrl = (url: string) => new URL(url, import.meta.url).href;

export const fetchCompanies = async () => {
  const fileUrl = getFileUrl('../db/Company.json')
  const { data } = await axios.get(fileUrl);
  return data.data as EcoCardBase[];
}

const targetDBs = {
  account: '../db/Account.json',
  group: '../db/Groups.json',
  shop: '../db/Shops.json'
}

export const fetchById = async<R extends Accounts | Groups | Shops>(idPair: [keyof R, string | number], target: keyof typeof targetDBs) => {
  const fileUrl = getFileUrl(targetDBs[target]);
  const [id, value] = idPair;
  const { data } = await axios.get(fileUrl);
  return data.data.filter((record: R) => record[id] === value) as R[];
}